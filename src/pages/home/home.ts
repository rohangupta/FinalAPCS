import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MetcalfesLaw } from '../../models/metcalfesLaw.model';
import { QuantityTheoryOfMoney } from '../../models/quantityTheoryOfMoney.model';
import { Runnable } from '../../models/runnable';
import { Changeable } from '../../models/changeable';
import { DCF } from '../../models/DCF.model';
import { PercentageOfMarket } from '../../models/percentageOfMarket.model';
import { MockData } from '../../models/mockData';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  MOCK_DATA: MockData = new MockData;
  //MetcalfesLaw
  YEAR = 2018;
  PERCENTAGE = .5;
  M3 = this.MOCK_DATA.M3;
  NUM_USERS = this.MOCK_DATA.NUM_BTC_USERS;
  EXPONENT = this.MOCK_DATA.EXPONENT;
  BTC_SUPPLY = this.MOCK_DATA.BTC_SUPPLY;
  TOTAL_TRANSACTION_VALUE = this.MOCK_DATA.TOTAL_TRANSACTIONS_VALUE;
  VELOCITY = this.MOCK_DATA.VELOCITY;
  O_CONST = (Math.pow(0.015,this.VELOCITY))/Math.sqrt(this.BTC_SUPPLY);
 //


  //DCF
  CASHFLOW = this.MOCK_DATA.CASHFLOW;
  LONGTERM = this.MOCK_DATA.LONGTERM;
  WACC = this.MOCK_DATA.WACC;
//

  metcalfesLaw: MetcalfesLaw;
  quantityTheoryOfMoney: QuantityTheoryOfMoney;
  dcf:DCF;
  percentageOfMarket: PercentageOfMarket;
  prices: Array<any>;

  simulate(runnable: Runnable): string {
    const simprice =  Number.parseFloat(runnable.run().toString()).toFixed(2);
    return simprice;
  }

  constructor(public navCtrl: NavController) {
    this.metcalfesLaw = new MetcalfesLaw (this.NUM_USERS, this.O_CONST, this.EXPONENT, this.BTC_SUPPLY);
    this.quantityTheoryOfMoney = new QuantityTheoryOfMoney (this.TOTAL_TRANSACTION_VALUE, this.VELOCITY, this.BTC_SUPPLY);
    this.percentageOfMarket = new PercentageOfMarket (this.YEAR, this.PERCENTAGE, this.M3);

    //this.CF.push(55);this.CF.push(60.5);this.CF.push(63.53);this.CF.push(66.7);this.CF.push(70.04);
    //this.R.push(1.08);this.R.push(1.08);this.R.push(1.08);this.R.push(1.08);this.R.push(1.08);
    this.dcf = new DCF(this.CASHFLOW,this.WACC,this.BTC_SUPPLY,this.LONGTERM);
    this.prices = [
      {
        title: 'Metcalfes Law',
        simPrice: this.simulate(this.metcalfesLaw),
        time: 'Now'
      },
      {
        title: 'Quantity Theory of Money',
        simPrice: this.simulate(this.quantityTheoryOfMoney),
        time: 'Now'
      },
      {
        title: 'DCF',
        simPrice: Number.parseFloat(this.dcf.run().toString()).toFixed(2),
        time: this.dcf.cashFlow.length.toString() + " years from NOW"
      },
      {
        title: 'Percentage of Market',
        simPrice: this.simulate(this.percentageOfMarket),
        time: "in " + this.percentageOfMarket.year.toString()
      },
    ]


  }



}
