import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GBM } from '../../models/GBM.model';
import { Runnable } from '../../models/runnable';
import { MockData } from '../../models/mockData';


@Component({
  selector: 'page-ranges',
  templateUrl: 'ranges.html'
})
export class RangesPage {
  MOCK_DATA: MockData = new MockData;
  OPENS = this.MOCK_DATA.OPENS;
  CLOSES = this.MOCK_DATA.CLOSES;
  GBM: GBM;
  DAYS = this.MOCK_DATA.SIM_YEARS * 365;
  SIM_TIMES = 60000;
  ranges: Array<any>;


  simHigh(runnable: Runnable): string {
    const high =  Number.parseFloat(runnable.run()[1].toString()).toFixed(2);
    return high;
  }

  simLow(runnable: Runnable): string {
    const low = Number.parseFloat(runnable.run()[0].toString()).toFixed(2);
    return low;
  }


  constructor(public navCtrl: NavController) {

    this.GBM = new GBM(this.OPENS, this.CLOSES, this.DAYS, this.SIM_TIMES);

    this.ranges = [
      {
        title: 'Geometric Brownian Motion',
        simLow: this.simLow(this.GBM),
        simHigh: this.simHigh(this.GBM),
        time: this.MOCK_DATA.SIM_YEARS + " years from NOW"
      }
    ]

  }

}
