import { Component } from '@angular/core';

import { RangesPage } from '../ranges/ranges';
import { ContactPage } from '../charts/charts';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = RangesPage;
  tab3Root = ContactPage;

  constructor() {

  }

}
