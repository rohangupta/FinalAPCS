import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { file } from '../charts/file';
import { MockData } from '../../models/mockData';

@Component({
  selector: 'page-contact',
  templateUrl: 'charts.html'
})
export class ContactPage {
  mockData = new MockData;
  public lineChartData:Array<any> = this.getData();

  public lineChartLabels:Array<any> = this.getTime();
  public lineChartOptions:any = {
    responsive: true,
    maintainAspectRatio: true,
  };
  public lineChartColors:Array<any> = [

    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },

  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public getData():Array<any>{
    //const fs = require('fs');

    console.log(this.mockData.CHART_DATA);
    return  [{data: this.mockData.CHART_DATA, label: 'Bitcoin Market Cap'}];
  }

  public getTime():Array<any>{

    return ['Jan17', 'Feb17', 'Mar17', 'Apr17', 'May17', 'Jun17', 'Jul17','Aug17','Sep17','Oct17','Nov17','Dec17','Jan18', 'Feb18', 'Mar18', 'Apr18', 'May18'];
  }


  // events
  public chartClicked(e:any):void {


    //this.randomize();
    console.log(e);
  }

  public chartHovered(e:any):void {

    console.log(e);
  }

  constructor(public navCtrl: NavController) {

  }

}
