import { Runnable } from '../models/runnable';
import { DataSet } from '../models/dataSet.model';

/*
  This algorithm works like this:  user inputs Money Velocity, total BTC Supply,
*/

export class QuantityTheoryOfMoney implements Runnable {

  totalTransactionsValue: number;
  velocity: number;
  btcValue: number;
  btcSupply: number;

  constructor(
    transactionsValue: number,
    v: number,
    supply: number
  ) {
    this.totalTransactionsValue = transactionsValue;
    this.btcSupply = supply;
    this.velocity = v;
  }

  run(): number {
    this.btcValue = (this.totalTransactionsValue) / (this.btcSupply * this.velocity) ;
    return this.btcValue;
  }




}
