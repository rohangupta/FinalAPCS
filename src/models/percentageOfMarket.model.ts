import { Runnable } from '../models/runnable';
import { DataSet } from '../models/dataSet.model';

/*
  This algorithm works like this:  user inputs an O-constant
*/

export class PercentageOfMarket implements Runnable {

  year: number;
  predictedPercentage: number;
  currentM3: number;
  btcValue: number;

  /** BTC end-of-year supplies (predicted) from 2018 to 2140 */

  /** algorithm for finding out year: year-2017 */
  suppliesByYear: Array<number> = [
    17718750.00, // EOY 2018
    18375000.00, // EOY 2019
    18703125.00, // EOY 2020
    19031250.00, // EOY 2021
    19359375.00, // EOY 2022
    19687500.00, // EOY 2023
    19851562.50, // EOY 2024
    20015625.00, // EOY 2025
    20179687.50, // EOY 2026
    20343750.00, // EOY 2027
    20425781.25, // EOY 2028
    20507812.50, // EOY 2029
    20589843.75, // EOY 2030
    20671875.00, // EOY 20
    20712890.63, // EOY 20
    20753906.25, // EOY 20
    20794921.88, // EOY 20
    20835937.50, // EOY 20
    20856445.31, // EOY 20
    20876953.13, // EOY 20
    20897460.94, // EOY 20
    20917968.75, // EOY 20
    20928222.66, // EOY 20
    20938476.56, // EOY 20
    20948730.47, // EOY 20
    20958984.38, // EOY 20
    20964111.33, // EOY 20
    20969238.28, // EOY 20
    20974365.23, // EOY 20
    20979492.19, // EOY 20
    20982055.66, // EOY 20
    20984619.14, // EOY 20
    20987182.62, // EOY 20
    20989746.09, // EOY 20
    20991027.83, // EOY 20
    20992309.57, // EOY 20
    20993591.31, // EOY 20
    20994873.05, // EOY 20
    20995513.92, // EOY 20
    20996154.79, // EOY 20
    20996795.65, // EOY 20
    20997436.52, // EOY 20
    20997756.96, // EOY 20
    20998077.39, // EOY 20
    20998397.83, // EOY 20
    20998718.26, // EOY 20
    20998878.48, // EOY 20
    20999038.70, // EOY 20
    20999198.91, // EOY 20
    20999359.13, // EOY 20
    20999439.24, // EOY 20
    20999519.35, // EOY 20
    20999599.46, // EOY 20
    20999679.57, // EOY 20
    20999719.62, // EOY 20
    20999759.67, // EOY 20
    20999799.73, // EOY 20
    20999839.78, // EOY 20
    20999859.81, // EOY 20
    20999879.84, // EOY 20
    20999899.86, // EOY 20
    20999919.89, // EOY 20
    20999929.90, // EOY 20
    20999939.92, // EOY 20
    20999949.93, // EOY 20
    20999959.95, // EOY 20
    20999964.95, // EOY 20
    20999969.96, // EOY 20
    20999974.97, // EOY 20
    20999979.97, // EOY 20
    20999982.48, // EOY 20
    20999984.98, // EOY 20
    20999987.48, // EOY 20
    20999989.99, // EOY 20
    20999991.24, // EOY 20
    20999992.49, // EOY 20
    20999993.74, // EOY 20
    20999994.99, // EOY 20
    20999995.62, // EOY 20
    20999996.24, // EOY 20
    20999996.87, // EOY 20
    20999997.50, // EOY 20
    20999997.81, // EOY 21
    20999998.12, // EOY 21
    20999998.44, // EOY 21
    20999998.75, // EOY 21
    20999998.90, // EOY 21
    20999999.06, // EOY 21
    20999999.22, // EOY 21
    20999999.37, // EOY 21
    20999999.45, // EOY 21
    20999999.53, // EOY 21
    20999999.61, // EOY 21
    20999999.69, // EOY 21
    20999999.73, // EOY 21
    20999999.77, // EOY 21
    20999999.80, // EOY 21
    20999999.84, // EOY 21
    20999999.86, // EOY 21
    20999999.88, // EOY 21
    20999999.90, // EOY 21
    20999999.92, // EOY 21
    20999999.93, // EOY 21
    20999999.94, // EOY 21
    20999999.95, // EOY 21
    20999999.96, // EOY 21
    20999999.97, // EOY 21
    20999999.97, // EOY 21
    20999999.98, // EOY 21
    20999999.98, // EOY 21
    20999999.98, // EOY 21
    20999999.99, // EOY 21
    20999999.99, // EOY 21
    20999999.99, // EOY 21
    20999999.99, // EOY 21
    20999999.99, // EOY 21
    20999999.99, // EOY 21
    21000000.00, // EOY 21
    21000000.00, // EOY 21
    21000000.00, // EOY 21
    21000000.00, // EOY 21
    21000000.00, // EOY 21
    21000000.00, // EOY 21
      ];

 constructor(
    yr: number,
    percentage: number,
    m3: number
  ) {
    this.year = yr;
    this.predictedPercentage = percentage;
    this.currentM3 = m3;
  }

  getSupply(): number {
    return this.suppliesByYear[this.year-2018];
  }
  run(): number {
    const realPercent = this.predictedPercentage/100;
    const marketCap = realPercent * this.currentM3;
    this.btcValue = marketCap / this.getSupply();


    return this.btcValue;
  }




}
