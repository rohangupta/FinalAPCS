import { Runnable } from '../models/runnable';
import { DataSet } from '../models/dataSet.model';

/*
  This algorithm works like this:  user inputs an O-constant
*/
export class DCF implements Runnable {
  years : number;

  cashFlow : Array<number>;
  discountRate:number;
  coins : number;
  lt :number;

  constructor(CF:Array<number>,Wacc : number, c: number,longterm :number){
    this.cashFlow = CF;
    this.discountRate = Wacc;
    this.years = CF.length;
    this.coins = c;
    this.lt = longterm;
  }

  run(): number {
    var total = 0;
    for(var i = 0; i < this.years;i++){
      total += this.cashFlow[i]/Math.pow(1+this.discountRate,i+1);
      console.log(this.cashFlow[i]/Math.pow(1+this.discountRate,i+1));
    }
    var terminalValue =  this.cashFlow[this.cashFlow.length-1]*(1+this.lt)/(this.discountRate-this.lt);
    terminalValue = terminalValue/Math.pow(1+this.discountRate,this.years);
    total+= terminalValue
    console.log(terminalValue);
    return total/this.coins;
  }
}
