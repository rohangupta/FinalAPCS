export class DataSet {

  dataSet: Array<number>;

  constructor(data: Array<number>) {
    this.dataSet = data;
  }

  getStandardDeviation(): number {
    const mean = this.getMean();

    const numTerms = this.dataSet.length;
    let sum = 0;
    this.dataSet.forEach(num => {
      const curr = Math.pow((num-mean),2);
      sum+=curr;
    });

    const final = Math.sqrt(sum/(numTerms));
    return final;
  }

  length(): number {
    return this.dataSet.length;
  }

  // returns the mean of dataSet
	getMean(): number {
    let total = 0;
    this.dataSet.forEach(num => {
      total+=num;
    });
		return total/this.dataSet.length;
  }

  // returns the mode of dataSet
  getMode(): number[] {
    let max = 0;
    let tally: Map<number, number>;
    let ret: Array<number> = [0];
    this.dataSet.forEach(num => {
      if(tally.has(num)) {
        tally.set(num, tally.get(num)+1);
        if(tally.get(num) > max) {
          max = tally.get(num);
        }
      } else {
        tally.set(num, 1);
      }
    });
    tally.forEach(tal => {
      if(tally.get(tal) == max) {
        ret.push(tal);
      }
    });

    return ret;
  }

  // returns the max of the dataSet
  getMax(): number {
    let max = Number.NEGATIVE_INFINITY;
    this.dataSet.forEach(num => {
      if(num > max) {
        max = num;
      }
    });

    return max;
  }

  // returns the min of the dataSet
  getMin(): number {
    let min = Number.POSITIVE_INFINITY;
    this.dataSet.forEach(num => {
      if(num < min) {
        min = num;
      }
    });

    return min;
  }

  // returns the median of the dataSet
  getMedian(): number {
    if(this.dataSet.length%2 ==1) {
        return this.dataSet[(this.dataSet.length-1)/2];
      } else {
      	return (this.dataSet[this.dataSet.length/2]+this.dataSet[this.dataSet.length/2-1])/2;
      }
  }

  // returns the range of the dataSet
	getRange(): number {
    return this.getMax() - this.getMin();
	}

  get(index: number): number {
   return this.dataSet[index];
  }

  add(num: number): void {
    this.dataSet.push(num);
  }

  sort(): void {
      var len = this.dataSet.length;
      for (var i = len-1; i>=0; i--){
        for(var j = 1; j<=i; j++){
          if(this.dataSet[j-1]>this.dataSet[j]){
              var temp = this.dataSet[j-1];
              this.dataSet[j-1] = this.dataSet[j];
              this.dataSet[j] = temp;
           }
        }
      }
   }

  valueAtPercentile(p: number): number {
    if(p > 100 || p < 0) {
      return null;
    }
    let index = (p*this.dataSet.length)/100;
    index = Math.floor(index) - 1;
    return this.dataSet[index];
  }

}
