import { DataSet } from '../models/dataSet.model';

// The interface for Runnables
export interface Runnable {
  run(): number | Array<number>;
}
