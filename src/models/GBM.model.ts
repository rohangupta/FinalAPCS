import { Runnable } from '../models/runnable';
import { DataSet } from '../models/dataSet.model';
/*

*/

export class PricesPair {
  open: number;
  close: number;

  constructor(o: number, c: number) {
    this.open = o;
    this.close = c;
  }
}

export class GBM implements Runnable {

  data: DataSet;
  priceHistory: Array<PricesPair>;
  returns: DataSet;
  logReturns: Array<number> = [];
  simulationDays: number;
  singleSim: Array<number> = [];
  singleSimPrice: number;
  opens: DataSet;
  closes: DataSet;
  monteCarlo: DataSet;
  monteCarloSims: Array<number> = [];
  priceLow: number;
  priceHigh: number;
  numSim: number;

  constructor(
    opens: Array<number>,
    closes: Array<number>,
    days: number,
    simNum: number
  ) {
    this.numSim = simNum;
    this.simulationDays = days;
    this.opens = new DataSet(opens);
    this.closes = new DataSet(closes);
    this.priceHistory = this.setUpPriceHistory();
    this.returns = new DataSet(this.getReturns());
    this.monteCarloSim();
  }

  setLogReturns(): void {
    // Where the randomness occurs due to this.getNormInv();
    this.logReturns = [];
    const meanDrift = this.getDrift('MEAN');
    const dailyVol = this.getVolatility('DAILY');
    for (let i = 0; i < this.simulationDays; i++) {
      let currLogReturn = meanDrift + (dailyVol * this.getNormInv());
      this.logReturns.push(currLogReturn);
    }
  }

  simulatePrices(): void {
    this.singleSim = [];
    let close = this.priceHistory[0].close;
    this.singleSim.push(close);
    for (let i = 0; i < this.simulationDays-1; i++) {
      let currDaySimPrice = this.singleSim[i] * Math.exp(this.logReturns[i+1]);
      this.singleSim.push(currDaySimPrice);
    }
    this.singleSimPrice = this.singleSim[this.singleSim.length-1];
  }

  simulatePricesTwo(): void {
    let close = this.priceHistory[0].close;
    let annVol = this.getVolatility('ANNUAL');
    let annDrift = this.getDrift('ANNUAL');
    let partOne = annDrift - 0.5*(Math.pow(annVol,2));
    let final = partOne+annDrift*this.getNormInv();
    final = close * Math.exp(final);
    this.singleSimPrice = final;
  }

  monteCarloSim(): void {
    for(let i = 0; i < this.numSim; i++) {
      this.setLogReturns();
      this.simulatePricesTwo();
      this.monteCarloSims.push(this.singleSimPrice);
    }
    this.monteCarlo = new DataSet(this.monteCarloSims);
  }

  getNormInv(): number {
    // randomizes normal inverse distribution
    let p = Math.random();

    var a =
      [
        -3.969683028665376e+01,
        2.209460984245205e+02,
        -2.759285104469687e+02,
        1.383577518672690e+02,
        -3.066479806614716e+01,
        2.506628277459239e+00
      ];
    var b =
      [
        -5.447609879822406e+01,
        1.615858368580409e+02,
        -1.556989798598866e+02,
        6.680131188771972e+01,
        -1.328068155288572e+01
      ];

    var c =
      [
        -7.784894002430293e-03,
        -3.223964580411365e-01,
        -2.400758277161838,
        -2.549732539343734,
        4.374664141464968,
        2.938163982698783
      ];

    var d =
      [
        7.784695709041462e-03,
        3.224671290700398e-01,
        2.445134137142996,
        3.754408661907416
      ];

    var LOW = 0.02425;
    var HIGH = 0.97575;
    var q, r;

    // errno = 0;

    if (p < LOW)
    {
      /* Rational approximation for lower region */
      q = Math.sqrt(-2*Math.log(p));
      return (((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
        ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
    }
    else if (p > HIGH)
    {
      /* Rational approximation for upper region */
      q  = Math.sqrt(-2*Math.log(1-p));
      return -(((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
        ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
    }
    else
    {
      /* Rational approximation for central region */
          q = p - 0.5;
          r = q*q;
      return (((((a[0]*r+a[1])*r+a[2])*r+a[3])*r+a[4])*r+a[5])*q /
        (((((b[0]*r+b[1])*r+b[2])*r+b[3])*r+b[4])*r+1);
    }

  }

  getVolatility(type: 'DAILY' | 'ANNUAL'): number {
    if (type == 'DAILY') {
      return this.returns.getStandardDeviation();
    } else if (type == 'ANNUAL') {
      const dailyVol = this.getVolatility('DAILY');
      return dailyVol * Math.sqrt(365);
    }
  }

  getDrift(type: 'DAILY' | 'ANNUAL' | 'MEAN'): number {
    if (type == 'DAILY') {
      return this.returns.getMean();
    } else if (type == 'ANNUAL') {
      const dailyDrift = this.getDrift('DAILY');
      return dailyDrift * 365;
    } else if (type == 'MEAN') {
      const dailyDrift = this.getDrift('DAILY');
      const dailyVol = this.getVolatility('DAILY');
      return dailyDrift * (0.5 * Math.pow(dailyVol, 2));
    }
  }

  run(): Array<number> {
    this.monteCarlo.sort();
    this.priceLow = this.monteCarlo.valueAtPercentile(12.5);
    this.priceHigh = this.monteCarlo.valueAtPercentile(87.5);
    return [this.priceLow, this.priceHigh];
  }

  getReturns(): Array<number> {
    let rets: Array<number> = [];
    this.priceHistory.forEach(pair => {
      let currReturn = Math.log(pair.close/pair.open);
      rets.push(currReturn);
    });
    return rets;
  }

  setUpPriceHistory(): Array<PricesPair> {
    let pHistory: Array<PricesPair> = [];
    for(let i = 0; i < this.opens.length(); i++) {
      let currPair = new PricesPair(this.opens.get(i), this.closes.get(i));
      pHistory.push(currPair);
    }

    return pHistory;

  }

}



