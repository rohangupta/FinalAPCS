import { Runnable } from '../models/runnable';
import { DataSet } from '../models/dataSet.model';

/*
  This algorithm works like this:  user inputs an O-constant
*/

export class MetcalfesLaw implements Runnable {

  usersNum: number;
  oConst: number;
  exponent: number;
  btcValue: number;
  btcSupply: number;

  constructor(
    numUsers: number,
    oConstant: number,
    exp: number,
    supply: number
  ) {
    this.usersNum = numUsers;
    this.oConst = oConstant;
    this.exponent = exp;
    this.btcSupply = supply;
  }

  run(): number {
    this.btcValue =
        (this.oConst) *
        (
          (Math.pow(this.usersNum,this.exponent) ) /*/ ( Math.sqrt(this.btcSupply) )*/
        );
    return this.btcValue;
  }




}
