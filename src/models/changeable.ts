import { Runnable } from "./runnable";

export interface Changeable {

  variables: Array<any>;
  finalVal: Runnable;

}
